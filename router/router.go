package router

import (
	"ms/goserver/config"
	"ms/goserver/handler"

	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {
	app.Get("/", handler.Root)

	api := app.Group("/api")
	api.Get("/update", handler.Update)
	api.Get("/update/:hotel", handler.UpdateHotel)

	auth := app.Group("/auth")
	auth.Get("/login", handler.Login)
	auth.Get("/logout", handler.Logout)

	dashboard := app.Group("dashboard")
	//loop over hotels and create static server protected by auth
	dashboard.Static("/reposName", config.DirToServe+"/reposName")

}
