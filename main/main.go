package main

import (
	"log"
	"ms/goserver/config"
	"ms/goserver/router"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/django"
)

func main() {

	engine := django.New("./views", ".html")
	app := fiber.New(fiber.Config{
		Views:             engine,
		EnablePrintRoutes: true,
	})

	if config.Read() != nil {
		log.Fatalln("Unable to read the config ...")
	}

	router.SetupRoutes(app)

	app.Listen(config.Host)
}
