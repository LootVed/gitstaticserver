module ms/goserver

go 1.18

// go get -u github.com/gofiber/fiber/v2
// go get -u github.com/gofiber/template
// go get -u github.com/gofiber/template/django
//# https://github.com/go-git/go-git

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/flosch/pongo2/v4 v4.0.2 // indirect
	github.com/gofiber/fiber/v2 v2.31.0 // indirect
	github.com/gofiber/template v1.6.26 // indirect
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.35.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220412071739-889880a91fd5 // indirect
)
