package handler

import "github.com/gofiber/fiber/v2"

func Root(c *fiber.Ctx) error {
	return c.Render("index", fiber.Map{"Message": "Hello, World!"})
}
